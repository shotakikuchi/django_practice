参考サイト : http://qiita.com/kaki_k/items/511611cadac1d0c69c54
==============================================================

## Python3なTensorFlow環境構築 (Macとpyenv virtualenv)
## http://qiita.com/hatapu/items/054dbab03607c47cb84f

## HomeBrewが必要なので、無ければインストール
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

## pyenv : 複数バージョンのPython環境を管理して、コマンドで切り替えることができる。
brew install pyenv

## ~/.profileに下記を追記
export PATH="$HOME/.pyenv/bin:$PATH"
eval "$(pyenv init -)"

## 現在のPythonインストール状況を確認（環境構築前ならシステムのみが表示されている）
pyenv versions
## * system (set by /Users/kikuchishota/.pyenv/version)

## インストール可能なPythonバージョンの一覧表示（結果は必要そうなとこだけ表示）
pyenv install --list
#2.7.11
#3.5.1

## 最新のPython3をインストール
pyenv install 3.5.1

## こういうエラーが出た場合は
## zipimport.ZipImportError: can't decompress data; zlib not available
## make: *** [install] Error 1

## コマンドラインツールをインストール
## xcode-select --install

## 設定読み直して
##  . .profile

## インストールやり直す
## pyenv install 3.5.1

## インストール後はrehashした方が良いらしい
pyenv rehash

## 開発環境を3.5.1に設定して確認
pyenv local 3.5.1
pyenv versions

## anacondaのインストール
pyenv install anaconda3-4.0.0

## インストール後のrehash
pyenv rehash

## pyenv-virtualenvのインストール
## pyenvだとバージョンごとに管理だったが、pyenvの環境に名前を付けて更に細かく管理して、切り替えることができる
brew install pyenv-virtualenv

## 今回はPython3.5.1でdjangoの環境を作りたいのでdjango1という名前を付けておきましょう。
## インストールと確認は下記の通りです。
pyenv virtualenv 3.5.1 django1

## 先ほどpyenv-virtualenvでインストールしたpy351tensorflow環境を使います
pyenv local django1
pyenv version
## django1  (set by /Users/kikuchishota/Documents/MyGithubRepository/Study/python/django_study/.python-version)

## Djangoのインストール
pip install django

## Django mybookプロジェクトの作成
django-admin.py startproject mybook



